APP=asciinema
DOMAIN='asciinema.example.org'

# Find the latest version at:
# https://github.com/asciinema/asciinema-server/releases
VERSION='20241103'

ADMIN_EMAIL='admin@example.org'

# Database settings.
DB_HOST='postgresql'
DB_NAME='asciinema'
DB_USER='asciinema'
DB_PASS='pass123'

# max size of the uploaded files
UPLOAD_SIZE_LIMIT=16M
