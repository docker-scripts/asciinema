cmd_backup_help() {
    cat <<_EOF
    backup
        Make a backup.

_EOF
}

cmd_backup() {
    # create the backup dir
    local backup="backup-$(date +%Y%m%d)"
    rm -rf $backup
    rm -f $backup.tgz
    mkdir $backup

    # dump the content of the database
    ds postgresql dump > $backup/postgres.sql

    cp settings.sh compose.env compose.yml $backup/
    cp -a data/uploads/ $backup/

    # make the backup archive
    tar --create --gzip --preserve-permissions --file=$backup.tgz $backup/
    rm -rf $backup/
}
