cmd_restore_help() {
    cat <<_EOF
    restore <backup-file.tgz> [-c]
        Restore from the given backup file.
        If the '-c' option is given, restore the config files as well.
        By default only the postgresql data and uploads are restored.

_EOF
}

cmd_restore() {
    local file=$1
    [[ ! -f $file ]] && fail "Usage:\n$(cmd_restore_help)"    
    local backup=${file%%.tgz}
    backup=$(basename $backup)
    local restore_config=$2

    # extract the backup archive
    tar --extract --gunzip --preserve-permissions --file=$file

    # stop the container
    ds stop

    # restore uploads
    rsync -a $backup/uploads/ data/uploads/

    # restore config files
    if [[ $restore_config == '-c' ]]; then
        cp $backup/settings.sh .
        cp $backup/compose.env .
        cp $backup/compose.yml .

        ds make
        ds stop
    fi

    # restore the content of the database
    ds postgresql script $backup/postgres.sql \

    # clean up
    rm -rf $backup

    # start
    ds start
}
