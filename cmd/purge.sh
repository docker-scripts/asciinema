cmd_purge_help() {
    cat <<_EOF
    purge
        Remove and cleanup all data and config files.

_EOF
}

cmd_purge() {
    ds remove
    ds revproxy rm
    ds postgresql drop

    rm -rf data/
    rm compose.*
}
