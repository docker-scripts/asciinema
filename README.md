# asciinema

## Installation

  - First, make sure that `ds`, `revproxy` and `postgresql` are
    installed:
    + https://gitlab.com/docker-scripts/ds#installation
    + https://gitlab.com/docker-scripts/revproxy#installation
    + https://gitlab.com/docker-scripts/postgresql#installation

  - Prepare:
  
    ```bash
    ds pull asciinema
    ds init asciinema @asciinema.example.org
    cd /var/ds/asciinema.example.org/
    vim settings.sh
    ```

  - Build and start: `ds make`

  - Open https://asciinema.example.org

## Create a user

User sign-up is disabled by default. It also requires an SMTP server.
To enable it follow these steps:

1. Edit `compose.env` and set SMTP variables. For more details see:
   https://docs.asciinema.org/manual/server/self-hosting/configuration/#email

2. Rebuild the container:

   ```bash
   docker compose down
   docker compose up -d
   ```

3. Send a test email:

   ```bash
   docker compose exec asciinema \
       send-test-email your@email.example.org
   ```

4. Edit `compose.env` and comment out `SIGN_UP_DISABLED=true`. Rebuild
   the container again.

5. Open https://asciinema.example.org and sign-up with your email
   address.

6. It is recommended to disable sign-up again. Uncomment
   `SIGN_UP_DISABLED=true` and rebuild the container.

7. Optionally, make your user an admin (although this has little value
   when there is a single user):

   ```bash
   docker compose exec asciinema \
       admin_add email@example.org
   ```

## Start recording

1. Install `asciinema` cli:

   ```bash
   apt install asciinema
   ```

2. Set the variable `ASCIINEMA_API_URL`:

   ```bash
   export ASCIINEMA_API_URL=https://asciinema.example.org
   ```

   Add it to `~/.bashrc`.

3. Authenticate to the asciinema server:

   ```bash
   asciinema auth
   ```

4. Make a record and upload it:

   ```bash
   asciinema rec demo.cast
   asciinema upload demo.cast
   ```

**TIP:** To automatically record all your sessions, add these lines
to `~/.bashrc` (or to `/etc/bash.bashrc`):

```bash
export ASCIINEMA_API_URL=https://asciinema.example.org
pstree $$ -s -a | grep -v grep | grep -q asciinema || {
    asciinema rec -q -i 3
    exit
}
```

## Update

Find the latest release at:  
https://github.com/asciinema/asciinema-server/releases

Then do:

```bash
ds update <version>
```