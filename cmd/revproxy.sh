rename_function cmd_revproxy global_cmd_revproxy
cmd_revproxy() {
    global_cmd_revproxy "$@"
    [[ $1 == 'add' ]] &&  _customize_revproxy_config
}

_customize_revproxy_config() {
    # replace https by http
    local type=$(ds revproxy type)
    local config_file=$(ds revproxy path)

    local upload_limit=${UPLOAD_SIZE_LIMIT:-2M}
    upload_limit=${upload_limit/B/}

    if [[ $type == 'nginx' ]]; then
        sed -i $config_file \
            -e "/proxy_params/ a \        client_max_body_size $upload_limit;" \
            -e "s#proxy_pass.*#proxy_pass http://$DOMAIN:4000;#"
    else   # apache2
        local apache_limit=$(numfmt --from=auto $upload_limit)
        sed -i $config_file \
            -e "/ProxyPass \/ / i \        LimitRequestBody $apache_limit" \
            -e "s#ProxyPass /.*#ProxyPass / http://$DOMAIN:4000/#" \
            -e "s#ProxyPassReverse /.*#ProxyPassReverse / http://$DOMAIN:4000/#"
    fi  

    # reload the new configuration
    ds @revproxy reload
}
