cmd_update_help() {
    cat <<_EOF
    update <version>
        Update to the given version. Find the latest version at:
        https://github.com/asciinema/asciinema-server/releases

_EOF
}

cmd_update() {
    local version=$1
    [[ -z $version ]] && fail "Usage:\n$(cmd_update_help)"    

    sed -i settings.sh \
        -e "/^VERSION=/c VERSION='$version'"

    sed -i compose.yml \
        -e "s/asciinema-server:.*/asciinema-server:$version/"

    docker compose down
    docker compose up -d
}
