#!/bin/bash

cmd_start() {
    docker compose start
}

cmd_stop() {
    docker compose stop
}

cmd_restart() {
    docker compose restart
}

cmd_shell() {
    docker compose exec -u root asciinema bash
}

cmd_remove() {
    docker compose down
    ds revproxy rm
}

cmd_make() {
    # format the upload limit in bytes
    local limit=${UPLOAD_SIZE_LIMIT:-2M}
    limit=${limit/B/}
    local upload_size_limit=$(numfmt --from=auto $limit)

    # create compose.yml
    cat <<EOF > compose.yml
services:
  asciinema:
    image: ghcr.io/asciinema/asciinema-server:$VERSION
    env_file: compose.env
    environment:
      - URL_HOST=$DOMAIN
      - URL_SCHEME=https
      - CONTACT_EMAIL_ADDRESS=$ADMIN_EMAIL
      - UPLOAD_SIZE_LIMIT=$upload_size_limit
    volumes:
      - ./data:/var/opt/asciinema
    restart: unless-stopped
    networks:
      docker-scripts:
        aliases:
          - $DOMAIN

networks:
  docker-scripts:
    name: $NETWORK
    external: true
EOF

    # create config file, if it does not exist
    if [[ ! -f compose.env ]]; then
        local secret_key_base=$(tr -cd '[:alnum:]' < /dev/urandom | head -c 64)
        cat <<EOF > compose.env
### For more details see:
### https://docs.asciinema.org/manual/server/self-hosting/configuration/

DATABASE_URL=postgresql://${DB_USER}:${DB_PASS}@${DB_HOST}:5432/${DB_NAME}
SECRET_KEY_BASE=$secret_key_base

#SMTP_HOST=smtp.example.org
#SMTP_PORT=25
#MAIL_FROM_ADDRESS=asciinema@example.org
#SMTP_USERNAME=foobar
#SMTP_PASSWORD=hunter2

SIGN_UP_DISABLED=true
#UNCLAIMED_RECORDING_TTL=30
EOF
        
        # create a database
        ds postgresql create
    fi

    # build and start the containers
    docker compose down
    docker compose up -d

    # add a revproxy configuration and get an ssl-cert
    ds revproxy add
    ds revproxy ssl-cert
}
